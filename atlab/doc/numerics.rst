Numerical functions and conversions: the :f:mod:`numerics` module
=================================================================

.. f:automodule:: numerics
