Finite grids in real space: the :f:mod:`box` module
===================================================

.. f:automodule:: box
