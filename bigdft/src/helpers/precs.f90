! +++++++++++++++++++++++++++++++++
! DON'T ADD ANYTHING TO THIS MODULE
! +++++++++++++++++++++++++++++++++
module module_precisions
  use f_precisions, only: gp => f_double, dp => f_double, wp => f_double, tp => f_double
  implicit none
end module module_precisions
