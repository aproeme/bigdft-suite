Installation
============

BigDFT can be installed directly using Docker or compiled from source. We
are also developing a light-weight client version of the code which will be
available soon.

Install via Docker
------------------

Docker is a virtualization platform that allows us to distribute BigDFT and
all of its associated dependencies in a single container. The BigDFT container
is available here_.

.. _here: https://hub.docker.com/r/bigdft/sdk//

Install from Source
-------------------

.. toctree::
   :maxdepth: 2

   source-install
   minimum-install
   errors-and-fixes