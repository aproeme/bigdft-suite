.. _errors and fixes:

Errors and Fixes
================

This page is a repository of common errors and their fixes. 

.. note::

	This page is still under construction, and is by no means an exhaustive list.	

.. raw:: html

   <hr>
   
Issues with autogen phase
=========================

Error: Autoreconf error during futile setup
-------------------------------------------

``*** Error during phase setup of futile: ########## Error running autoreconf -fi *** [1/1]``

Solution:

.. code:: bash

	sudo apt install autoconf
	
.. note::
	
	This may also indicate missing MPI libraries. Try:
	
	.. code:: bash
	
		sudo apt install openmpi-bin
	
.. raw:: html

   <hr>
   
Issues with build phase
=======================

Error: Missing C compiler:
--------------------------

``configure: error: no acceptable C compiler found in $PATH``

fixed by:

.. code:: bash

	sudo apt install build-essential
	
.. raw:: html

   <hr>
	
Error: Could not compile .f90 files
-----------------------------------

``configure: error: Fortran could not compile .f90 files``

Solution:

.. code:: bash

	sudo apt install gfortran
	
.. raw:: html

   <hr>
   
Error: Missing linear algebra libraries
---------------------------------------

``Linear algebra libraries are not available``

Occurs during ``futile`` phase

Solution:

.. code:: bash

	sudo apt install libblas-dev liblapack-dev
	
.. raw:: html

   <hr>
   
Error: Missing cmake
--------------------

``*** Error during phase configure of ntpoly: cmake not found *** [5/12]``

Occurs during ``ntpoly`` configure phase

may also contain::

	Could NOT find MPI_C (missing: MPI_C_LIB_NAMES MPI_C_HEADER_DIR MPI_C_WORKS)
	Could NOT find MPI_CXX (missing: MPI_CXX_LIB_NAMES MPI_CXX_HEADER_DIR MPI_CXX_WORKS)
	Could NOT find MPI_Fortran (missing: MPI_Fortran_LIB_NAMES MPI_Fortran_F77_HEADER_DIR MPI_Fortran_MODULE_DIR MPI_Fortran_WORKS)

Solution:

.. code:: bash

	sudo apt install cmake
	sudo apt install openmpi-bin
	
.. raw:: html

   <hr>
   
Error: Bigdft build phase make issues
-------------------------------------

```*** Error during phase build of bigdft: ########## Error running make -j 5  *** [12/12]```

This issue was fixed in an earlier release, however, if the following lines are present in the output::

	/usr/bin/ld: /home/pi/bigdft-test/build/install/lib/libfutile-1.a(f_plugin.o): undefined reference to symbol 'dlsym@@GLIBC_2.2.5'
	/usr/bin/ld: /lib/x86_64-linux-gnu/libdl.so.2: error adding symbols: DSO missing from command line
	collect2: error: ld returned 1 exit status

Try to add ``"LIBS=-ldl -lstdc++"`` to the env_configuration in the rcfile you are using to build.

.. raw:: html

   <hr>
   
Issues during usage
===================
   
Error: Missing "python"
-----------------------

``python not found``

This error indicates an issue with launching python3 from the python command.

If you used ``python3 Installer.py ...`` to install, a mismatch of python versions can be created.

The simplest solution is to install

.. code:: bash

	sudo apt install python-is-python3
	
and reinstall using ``python Installer.py <previous commands>``