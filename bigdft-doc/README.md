# BigDFT User documentation

This repository contains the sources for the user documentation of BigDFT code

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/l_sim%2Fbigdft-doc/devel?filepath=tutorials%2FTutorial-N2.ipynb)
