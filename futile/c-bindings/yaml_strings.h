#ifndef YAML_STRINGS_H
#define YAML_STRINGS_H

#include "futile_cst.h"
#include <stdbool.h>

F_DEFINE_TYPE(f_string);

f90_f_string_pointer f90_f_string_copy_constructor(const f90_f_string* other);

f90_f_string_pointer f90_f_string_type_new(const char* msg);

void f90_f_string_free(f90_f_string_pointer self);

f90_f_string_pointer f90_f_string_empty(void);

void yaml_bold(char out_bstr[95],
  const char* str);

void yaml_blink(char out_bstr[95],
  const char* str);

void yaml_date_and_time_toa(char out_yaml_date_and_time_toa[95],
  const int (*values)[8],
  const bool (*zone));

void yaml_date_toa(char out_yaml_date_toa[95],
  const int (*values)[8]);

void yaml_time_toa(char out_yaml_time_toa[95],
  const int (*values)[8]);

bool is_atoi(const char* str);

bool is_atoli(const char* str);

bool is_atof(const char* str);

bool is_atol(const char* str);

/* void f_char_ptr(const char* str); */

void buffer_string(char* string_bn,
  int string_lgt,
  const char* buffer,
  int* string_pos,
  const bool (*back),
  int (*istat));

void align_message(bool rigid,
  int maxlen,
  int tabval,
  const char* anchor,
  char* message);

void read_fraction_string(const char* string_bn,
  double* var,
  int* ierror);

void rstrip(char* string_bn,
  size_t string_bn_len,
  const char* substring);

void shiftstr(char* str,
  size_t str_len,
  int n);

void convert_f_char_ptr(const char* src,
  char* dest,
  size_t dest_len);

void yaml_itoa(char out_str[95],
  int data,
  const char (*fmt));

void yaml_litoa(char out_str[95],
  size_t data,
  const char (*fmt));

void yaml_ftoa(char out_str[95],
  float data,
  const char (*fmt));

void yaml_dtoa(char out_str[95],
  double data,
  const char (*fmt));

/* void yaml_ctoa(const char* d,
  const char (*fmt)); */

/* void yaml_ztoa(void); */

void yaml_ltoa(char out_yaml_ltoa[95],
  bool l,
  const char (*fmt));

void yaml_dvtoa(char out_vec_toa[95],
  const double* vec,
  size_t vec_dim_0,
  const char (*fmt));

/* void yaml_zvtoa(void); */

void yaml_ivtoa(char out_vec_toa[95],
  const int* vec,
  size_t vec_dim_0,
  const char (*fmt));

/* void yaml_cvtoa(void); */

/* void yaml_lvtoa(void); */

void yaml_rvtoa(char out_vec_toa[95],
  const float* vec,
  size_t vec_dim_0,
  const char (*fmt));

void yaml_livtoa(char out_vec_toa[95],
  const long* vec,
  size_t vec_dim_0,
  const char (*fmt));

void f_strcpy(char* dest,
  size_t dest_len,
  const char* src);

void f_strcpy_str(char* dest,
  size_t dest_len,
  const f90_f_string* src);

bool string_equivalence(const char* a,
  const char* b);

bool string_inequivalence(const char* a,
  const char* b);

f90_f_string_pointer string_and_integer(const char* a,
  int num);

f90_f_string_pointer integer_and_string(int a,
  const char* num);

f90_f_string_pointer integer_and_msg(int a,
  const f90_f_string* num);

f90_f_string_pointer string_and_long(const char* a,
  size_t num);

f90_f_string_pointer string_and_double(const char* a,
  double num);

f90_f_string_pointer string_and_simple(const char* a,
  float num);

/* void string_and_msg(const char* a,
  const f90_f_string* num); */

/* void msg_and_msg(const f90_f_string* a,
  const f90_f_string* num); */

/* void msg_and_string(const f90_f_string* a,
  const char* num); */

/* void combine_strings(const char* a,
  const char* b); */

/* void combine_msg(const f90_f_string* a,
  const f90_f_string* b); */

/* void attach_msg_c(const f90_f_string* a,
  const char* b); */

/* void attach_c_msg(const char* a,
  const f90_f_string* b); */

f90_f_string_pointer attach_ci(const char* s,
  int num);

f90_f_string_pointer attach_cli(const char* s,
  size_t num);

f90_f_string_pointer attach_lic(size_t num,
  const char* s);

f90_f_string_pointer attach_cd(const char* s,
  double num);

void msg_to_string(char* string_bn,
  size_t string_bn_len,
  const f90_f_string* msg);

void string_to_msg(f90_f_string* msg,
  const char* string_bn);

void yaml_itoa_fmt(char out_c[95],
  int num,
  const char* fmt);

void yaml_litoa_fmt(char out_c[95],
  size_t num,
  const char* fmt);

void yaml_dtoa_fmt(char out_c[95],
  double num,
  const char* fmt);

void yaml_ctoa_fmt(char out_c[95],
  const char* num,
  const char* fmt);

#endif
