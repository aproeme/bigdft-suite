#include "wrapper_mpi.h"
#include <config.h>
#include <string.h>

void FC_FUNC_(bind_f90_mpi_environment_copy_constructor, BIND_F90_MPI_ENVIRONMENT_COPY_CONSTRUCTOR)(f90_mpi_environment_pointer*,
  const f90_mpi_environment*);
f90_mpi_environment_pointer f90_mpi_environment_copy_constructor(const f90_mpi_environment* other)
{
  f90_mpi_environment_pointer out_self;
  FC_FUNC_(bind_f90_mpi_environment_copy_constructor, BIND_F90_MPI_ENVIRONMENT_COPY_CONSTRUCTOR)
    (&out_self, other);
  return out_self;
}

void FC_FUNC_(bind_f90_mpi_environment_type_new, BIND_F90_MPI_ENVIRONMENT_TYPE_NEW)(f90_mpi_environment_pointer*,
  const f90_f_reference_counter*,
  const int*,
  const int*,
  const int*,
  const int*,
  const int*);
f90_mpi_environment_pointer f90_mpi_environment_type_new(const f90_f_reference_counter* refcnt,
  const int (*igroup),
  const int (*iproc),
  const int (*mpi_comm),
  const int (*ngroup),
  const int (*nproc))
{
  f90_mpi_environment_pointer out_self;
  FC_FUNC_(bind_f90_mpi_environment_type_new, BIND_F90_MPI_ENVIRONMENT_TYPE_NEW)
    (&out_self, refcnt, igroup, iproc, mpi_comm, ngroup, nproc);
  return out_self;
}

void FC_FUNC_(bind_f90_mpi_environment_free, BIND_F90_MPI_ENVIRONMENT_FREE)(f90_mpi_environment_pointer*);
void f90_mpi_environment_free(f90_mpi_environment_pointer self)
{
  FC_FUNC_(bind_f90_mpi_environment_free, BIND_F90_MPI_ENVIRONMENT_FREE)
    (&self);
}

void FC_FUNC_(bind_mpi_environment_null, BIND_MPI_ENVIRONMENT_NULL)(f90_mpi_environment_pointer*);
f90_mpi_environment_pointer mpi_environment_null(void)
{
  f90_mpi_environment_pointer out_mpi;
  FC_FUNC_(bind_mpi_environment_null, BIND_MPI_ENVIRONMENT_NULL)
    (&out_mpi);
  return out_mpi;
}

void FC_FUNC_(bind_mpi_environment_comm, BIND_MPI_ENVIRONMENT_COMM)(f90_mpi_environment_pointer*,
  const int*);
f90_mpi_environment_pointer mpi_environment_comm(const int (*comm))
{
  f90_mpi_environment_pointer out_mpi_env;
  FC_FUNC_(bind_mpi_environment_comm, BIND_MPI_ENVIRONMENT_COMM)
    (&out_mpi_env, comm);
  return out_mpi_env;
}

void FC_FUNC_(bind_release_mpi_environment, BIND_RELEASE_MPI_ENVIRONMENT)(f90_mpi_environment*);
void release_mpi_environment(f90_mpi_environment* mpi_env)
{
  FC_FUNC_(bind_release_mpi_environment, BIND_RELEASE_MPI_ENVIRONMENT)
    (mpi_env);
}

void FC_FUNC_(bind_mpi_environment_set, BIND_MPI_ENVIRONMENT_SET)(f90_mpi_environment*,
  const int*,
  const int*,
  const int*,
  const int*);
void mpi_environment_set(f90_mpi_environment* mpi_env,
  int iproc,
  int nproc,
  int mpi_comm,
  int groupsize)
{
  FC_FUNC_(bind_mpi_environment_set, BIND_MPI_ENVIRONMENT_SET)
    (mpi_env, &iproc, &nproc, &mpi_comm, &groupsize);
}

void FC_FUNC_(bind_mpi_environment_dict, BIND_MPI_ENVIRONMENT_DICT)(const f90_mpi_environment*,
  f90_dictionary_pointer*);
void mpi_environment_dict(const f90_mpi_environment* mpi_env,
  f90_dictionary_pointer dict_info)
{
  FC_FUNC_(bind_mpi_environment_dict, BIND_MPI_ENVIRONMENT_DICT)
    (mpi_env, &dict_info);
}

void FC_FUNC_(bind_mpi_environment_set1, BIND_MPI_ENVIRONMENT_SET1)(f90_mpi_environment*,
  const int*,
  const int*,
  const int*,
  const int*);
void mpi_environment_set1(f90_mpi_environment* mpi_env,
  int iproc,
  int mpi_comm,
  int groupsize,
  int ngroup)
{
  FC_FUNC_(bind_mpi_environment_set1, BIND_MPI_ENVIRONMENT_SET1)
    (mpi_env, &iproc, &mpi_comm, &groupsize, &ngroup);
}

