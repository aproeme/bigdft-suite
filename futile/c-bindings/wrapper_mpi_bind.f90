subroutine bind_f90_mpi_environment_copy_constructor( &
    out_self, &
    other)
  use f_onesided
  use f_refcnts
  use f_allreduce
  use time_profiling, only: timing_uninitialized
  use f_precisions
  use f_bcast
  use f_alltoall
  use mpif_module
  use dictionaries, only: f_err_throw
  use fmpi_types
  use yaml_strings
  use f_sendrecv
  use f_gather
  use f_allgather
  use wrapper_mpi
  implicit none
  type(mpi_environment), pointer :: out_self
  type(mpi_environment), intent(in) :: other

  nullify(out_self)
  allocate(out_self)
  out_self = other
end subroutine bind_f90_mpi_environment_copy_constructor

subroutine bind_f90_mpi_environment_type_new( &
    out_self, &
    refcnt, &
    igroup, &
    iproc, &
    mpi_comm, &
    ngroup, &
    nproc)
  use f_onesided
  use f_refcnts
  use f_allreduce
  use time_profiling, only: timing_uninitialized
  use f_precisions
  use f_bcast
  use f_alltoall
  use mpif_module
  use dictionaries, only: f_err_throw
  use fmpi_types
  use yaml_strings
  use f_sendrecv
  use f_gather
  use f_allgather
  use wrapper_mpi
  implicit none
  type(mpi_environment), pointer :: out_self
  type(f_reference_counter), intent(in) :: refcnt
  integer, optional, intent(in) :: igroup
  integer, optional, intent(in) :: iproc
  integer, optional, intent(in) :: mpi_comm
  integer, optional, intent(in) :: ngroup
  integer, optional, intent(in) :: nproc

  nullify(out_self)
  allocate(out_self)
  out_self%refcnt = refcnt
  if (present(igroup)) out_self%igroup = igroup
  if (present(iproc)) out_self%iproc = iproc
  if (present(mpi_comm)) out_self%mpi_comm = mpi_comm
  if (present(ngroup)) out_self%ngroup = ngroup
  if (present(nproc)) out_self%nproc = nproc
end subroutine bind_f90_mpi_environment_type_new

subroutine bind_f90_mpi_environment_free( &
    self)
  use f_onesided
  use f_refcnts
  use f_allreduce
  use time_profiling, only: timing_uninitialized
  use f_precisions
  use f_bcast
  use f_alltoall
  use mpif_module
  use dictionaries, only: f_err_throw
  use fmpi_types
  use yaml_strings
  use f_sendrecv
  use f_gather
  use f_allgather
  use wrapper_mpi
  implicit none
  type(mpi_environment), pointer :: self

  deallocate(self)
  nullify(self)
end subroutine bind_f90_mpi_environment_free

subroutine bind_mpi_environment_null( &
    out_mpi)
  use f_onesided
  use f_refcnts
  use f_allreduce
  use time_profiling, only: timing_uninitialized
  use f_precisions
  use f_bcast
  use f_alltoall
  use mpif_module
  use dictionaries, only: f_err_throw
  use fmpi_types
  use yaml_strings
  use f_sendrecv
  use f_gather
  use f_allgather
  use wrapper_mpi
  implicit none
  type(mpi_environment), pointer :: out_mpi

  nullify(out_mpi)
  allocate(out_mpi)
  out_mpi = mpi_environment_null( &
)
end subroutine bind_mpi_environment_null

subroutine bind_mpi_environment_comm( &
    out_mpi_env, &
    comm)
  use f_onesided
  use f_refcnts
  use f_allreduce
  use time_profiling, only: timing_uninitialized
  use f_precisions
  use f_bcast
  use f_alltoall
  use mpif_module
  use dictionaries, only: f_err_throw
  use fmpi_types
  use yaml_strings
  use f_sendrecv
  use f_gather
  use f_allgather
  use wrapper_mpi
  implicit none
  type(mpi_environment), pointer :: out_mpi_env
  integer(kind = fmpi_integer), optional, intent(in) :: comm

  nullify(out_mpi_env)
  allocate(out_mpi_env)
  out_mpi_env = mpi_environment_comm( &
    comm)
end subroutine bind_mpi_environment_comm

subroutine bind_release_mpi_environment( &
    mpi_env)
  use f_onesided
  use f_refcnts
  use f_allreduce
  use time_profiling, only: timing_uninitialized
  use f_precisions
  use f_bcast
  use f_alltoall
  use mpif_module
  use dictionaries, only: f_err_throw
  use fmpi_types
  use yaml_strings
  use f_sendrecv
  use f_gather
  use f_allgather
  use wrapper_mpi
  implicit none
  type(mpi_environment), intent(inout) :: mpi_env

  call release_mpi_environment( &
    mpi_env)
end subroutine bind_release_mpi_environment

subroutine bind_mpi_environment_set( &
    mpi_env, &
    iproc, &
    nproc, &
    mpi_comm, &
    groupsize)
  use f_onesided
  use f_refcnts
  use f_allreduce
  use time_profiling, only: timing_uninitialized
  use f_precisions
  use f_bcast
  use f_alltoall
  use mpif_module
  use dictionaries, only: f_err_throw
  use yaml_output
  use fmpi_types
  use yaml_strings
  use dynamic_memory
  use f_sendrecv
  use f_gather
  use f_allgather
  use wrapper_mpi
  implicit none
  type(mpi_environment), intent(out) :: mpi_env
  integer, intent(in) :: iproc
  integer, intent(in) :: nproc
  integer, intent(in) :: mpi_comm
  integer, intent(in) :: groupsize

  call mpi_environment_set( &
    mpi_env, &
    iproc, &
    nproc, &
    mpi_comm, &
    groupsize)
end subroutine bind_mpi_environment_set

subroutine bind_mpi_environment_dict( &
    mpi_env, &
    dict_info)
  use f_onesided
  use f_refcnts
  use f_allreduce
  use time_profiling, only: timing_uninitialized
  use f_precisions
  use f_bcast
  use f_alltoall
  use mpif_module
  use dictionaries
  use fmpi_types
  use yaml_strings
  use dynamic_memory
  use f_sendrecv
  use f_gather
  use f_allgather
  use wrapper_mpi
  implicit none
  type(mpi_environment), intent(in) :: mpi_env
  type(dictionary), pointer :: dict_info

  call mpi_environment_dict( &
    mpi_env, &
    dict_info)
end subroutine bind_mpi_environment_dict

subroutine bind_mpi_environment_set1( &
    mpi_env, &
    iproc, &
    mpi_comm, &
    groupsize, &
    ngroup)
  use f_onesided
  use f_refcnts
  use f_allreduce
  use time_profiling, only: timing_uninitialized
  use f_precisions
  use f_bcast
  use f_alltoall
  use mpif_module
  use dictionaries, only: f_err_throw
  use yaml_output
  use fmpi_types
  use yaml_strings
  use dynamic_memory
  use f_sendrecv
  use f_gather
  use f_allgather
  use wrapper_mpi
  implicit none
  type(mpi_environment), intent(out) :: mpi_env
  integer, intent(in) :: iproc
  integer, intent(in) :: mpi_comm
  integer, intent(in) :: groupsize
  integer, intent(in) :: ngroup

  call mpi_environment_set1( &
    mpi_env, &
    iproc, &
    mpi_comm, &
    groupsize, &
    ngroup)
end subroutine bind_mpi_environment_set1

