#include "FUtils"
#include <config.h>
#include <string.h>

using namespace Futile;

extern "C" {
void FC_FUNC_(bind_f90_f_progress_bar_copy_constructor, BIND_F90_F_PROGRESS_BAR_COPY_CONSTRUCTOR)(FProgressBar::f90_f_progress_bar_pointer*,
  const FProgressBar::f90_f_progress_bar*);
void FC_FUNC_(bind_f90_f_progress_bar_type_new, BIND_F90_F_PROGRESS_BAR_TYPE_NEW)(FProgressBar::f90_f_progress_bar_pointer*,
  const char*,
  const int*,
  const int*,
  const size_t*,
  size_t);
void FC_FUNC_(bind_f90_f_progress_bar_free, BIND_F90_F_PROGRESS_BAR_FREE)(FProgressBar::f90_f_progress_bar_pointer*);
void FC_FUNC_(bind_f90_f_progress_bar_empty, BIND_F90_F_PROGRESS_BAR_EMPTY)(FProgressBar::f90_f_progress_bar_pointer*);
void FC_FUNC_(bind_f90_f_none_object_copy_constructor, BIND_F90_F_NONE_OBJECT_COPY_CONSTRUCTOR)(FNoneObject::f90_f_none_object_pointer*,
  const FNoneObject::f90_f_none_object*);
void FC_FUNC_(bind_f90_f_none_object_type_new, BIND_F90_F_NONE_OBJECT_TYPE_NEW)(FNoneObject::f90_f_none_object_pointer*,
  const char*,
  const size_t*,
  size_t);
void FC_FUNC_(bind_f90_f_none_object_free, BIND_F90_F_NONE_OBJECT_FREE)(FNoneObject::f90_f_none_object_pointer*);
void FC_FUNC_(bind_f90_f_dump_buffer_copy_constructor, BIND_F90_F_DUMP_BUFFER_COPY_CONSTRUCTOR)(FDumpBuffer::f90_f_dump_buffer_pointer*,
  const FDumpBuffer::f90_f_dump_buffer*);
void FC_FUNC_(bind_f90_f_dump_buffer_type_new, BIND_F90_F_DUMP_BUFFER_TYPE_NEW)(FDumpBuffer::f90_f_dump_buffer_pointer*,
  const char*,
  const size_t*,
  const int*,
  size_t);
void FC_FUNC_(bind_f90_f_dump_buffer_free, BIND_F90_F_DUMP_BUFFER_FREE)(FDumpBuffer::f90_f_dump_buffer_pointer*);
void FC_FUNC_(bind_f90_f_dump_buffer_empty, BIND_F90_F_DUMP_BUFFER_EMPTY)(FDumpBuffer::f90_f_dump_buffer_pointer*);
void FC_FUNC_(bind_f_none, BIND_F_NONE)(FNoneObject::f90_f_none_object_pointer*);
void FC_FUNC_(bind_f_time, BIND_F_TIME)(size_t*);
void FC_FUNC_(bind_f_progress_bar_new, BIND_F_PROGRESS_BAR_NEW)(FProgressBar::f90_f_progress_bar_pointer*,
  const int*);
void FC_FUNC_(bind_f_tty, BIND_F_TTY)(int*,
  const int*);
void FC_FUNC_(bind_f_get_free_unit, BIND_F_GET_FREE_UNIT)(int*,
  const int*);
void FC_FUNC_(bind_f_getpid, BIND_F_GETPID)(int*);
void FC_FUNC_(bind_f_utils_errors, BIND_F_UTILS_ERRORS)(void);
void FC_FUNC_(bind_update_progress_bar, BIND_UPDATE_PROGRESS_BAR)(FProgressBar::f90_f_progress_bar*,
  const int*);
void FC_FUNC_(bind_f_pause, BIND_F_PAUSE)(const int*);
void FC_FUNC_(bind_f_utils_recl, BIND_F_UTILS_RECL)(const int*,
  const int*,
  int*);
void FC_FUNC_(bind_f_file_exists, BIND_F_FILE_EXISTS)(const char*,
  const size_t*,
  int*,
  size_t);
void FC_FUNC_(bind_f_close, BIND_F_CLOSE)(const int*);
void FC_FUNC_(bind_f_file_unit, BIND_F_FILE_UNIT)(const char*,
  const size_t*,
  int*,
  size_t);
void FC_FUNC_(bind_f_mkdir, BIND_F_MKDIR)(const char*,
  const size_t*,
  char*,
  const size_t*,
  size_t,
  size_t);
void FC_FUNC_(bind_f_delete_file, BIND_F_DELETE_FILE)(const char*,
  const size_t*,
  size_t);
void FC_FUNC_(bind_f_move_file, BIND_F_MOVE_FILE)(const char*,
  const size_t*,
  const char*,
  const size_t*,
  size_t,
  size_t);
void FC_FUNC_(bind_f_system, BIND_F_SYSTEM)(const char*,
  const size_t*,
  size_t);
void FC_FUNC_(bind_f_rewind, BIND_F_REWIND)(const int*);
void FC_FUNC_(bind_f_open_file, BIND_F_OPEN_FILE)(int*,
  const char*,
  const size_t*,
  const char*,
  const size_t*,
  const char*,
  const size_t*,
  const char*,
  const size_t*,
  const int*,
  size_t,
  size_t,
  size_t,
  size_t);
void FC_FUNC_(bind_f_diff_i, BIND_F_DIFF_I)(const size_t*,
  int*,
  int*,
  int*);
void FC_FUNC_(bind_f_diff_i2i1, BIND_F_DIFF_I2I1)(const size_t*,
  const int*,
  const size_t*,
  const size_t*,
  const int*,
  const size_t*,
  int*);
void FC_FUNC_(bind_f_diff_i3i1, BIND_F_DIFF_I3I1)(const size_t*,
  const int*,
  const size_t*,
  const size_t*,
  const size_t*,
  const int*,
  const size_t*,
  int*);
void FC_FUNC_(bind_f_diff_i2, BIND_F_DIFF_I2)(const size_t*,
  const int*,
  const size_t*,
  const size_t*,
  const int*,
  const size_t*,
  const size_t*,
  int*);
void FC_FUNC_(bind_f_diff_i1, BIND_F_DIFF_I1)(const size_t*,
  const int*,
  const size_t*,
  const int*,
  const size_t*,
  int*);
void FC_FUNC_(bind_f_diff_i1i2, BIND_F_DIFF_I1I2)(const size_t*,
  const int*,
  const size_t*,
  const int*,
  const size_t*,
  const size_t*,
  int*);
void FC_FUNC_(bind_f_diff_li, BIND_F_DIFF_LI)(const size_t*,
  long*,
  long*,
  long*);
void FC_FUNC_(bind_f_diff_li2li1, BIND_F_DIFF_LI2LI1)(const size_t*,
  const long*,
  const size_t*,
  const size_t*,
  const long*,
  const size_t*,
  long*);
void FC_FUNC_(bind_f_diff_li2, BIND_F_DIFF_LI2)(const size_t*,
  const long*,
  const size_t*,
  const size_t*,
  const long*,
  const size_t*,
  const size_t*,
  long*);
void FC_FUNC_(bind_f_diff_li1, BIND_F_DIFF_LI1)(const size_t*,
  const long*,
  const size_t*,
  const long*,
  const size_t*,
  long*);
void FC_FUNC_(bind_f_diff_li1li2, BIND_F_DIFF_LI1LI2)(const size_t*,
  const long*,
  const size_t*,
  const long*,
  const size_t*,
  const size_t*,
  long*);
void FC_FUNC_(bind_f_diff_r, BIND_F_DIFF_R)(const size_t*,
  float*,
  float*,
  float*);
void FC_FUNC_(bind_f_diff_d, BIND_F_DIFF_D)(const size_t*,
  double*,
  double*,
  double*);
void FC_FUNC_(bind_f_diff_d1, BIND_F_DIFF_D1)(const size_t*,
  const double*,
  const size_t*,
  const double*,
  const size_t*,
  double*,
  size_t*);
void FC_FUNC_(bind_f_diff_d2d3, BIND_F_DIFF_D2D3)(const size_t*,
  const double*,
  const size_t*,
  const size_t*,
  const double*,
  const size_t*,
  const size_t*,
  const size_t*,
  double*);
void FC_FUNC_(bind_f_diff_d2d1, BIND_F_DIFF_D2D1)(const size_t*,
  const double*,
  const size_t*,
  const size_t*,
  const double*,
  const size_t*,
  double*);
void FC_FUNC_(bind_f_diff_d3d1, BIND_F_DIFF_D3D1)(const size_t*,
  const double*,
  const size_t*,
  const size_t*,
  const size_t*,
  const double*,
  const size_t*,
  double*);
void FC_FUNC_(bind_f_diff_d0d1, BIND_F_DIFF_D0D1)(const size_t*,
  double*,
  const double*,
  const size_t*,
  double*);
void FC_FUNC_(bind_f_diff_d2, BIND_F_DIFF_D2)(const size_t*,
  const double*,
  const size_t*,
  const size_t*,
  const double*,
  const size_t*,
  const size_t*,
  double*);
void FC_FUNC_(bind_f_diff_d3, BIND_F_DIFF_D3)(const size_t*,
  const double*,
  const size_t*,
  const size_t*,
  const size_t*,
  const double*,
  const size_t*,
  const size_t*,
  const size_t*,
  double*);
void FC_FUNC_(bind_f_diff_d1d2, BIND_F_DIFF_D1D2)(const size_t*,
  const double*,
  const size_t*,
  const double*,
  const size_t*,
  const size_t*,
  double*);
void FC_FUNC_(bind_f_diff_c1i1, BIND_F_DIFF_C1I1)(const size_t*,
  const char*,
  const size_t*,
  const int*,
  const size_t*,
  int*,
  size_t);
void FC_FUNC_(bind_f_diff_c1li1, BIND_F_DIFF_C1LI1)(const size_t*,
  const char*,
  const size_t*,
  const size_t*,
  const size_t*,
  size_t*,
  size_t);
void FC_FUNC_(bind_f_diff_c0i1, BIND_F_DIFF_C0I1)(const size_t*,
  const char*,
  const size_t*,
  const int*,
  const size_t*,
  int*,
  size_t);
void FC_FUNC_(bind_f_diff_c0li1, BIND_F_DIFF_C0LI1)(const size_t*,
  const char*,
  const size_t*,
  const size_t*,
  const size_t*,
  size_t*,
  size_t);
void FC_FUNC_(bind_f_diff_li0li1, BIND_F_DIFF_LI0LI1)(const size_t*,
  size_t*,
  const size_t*,
  const size_t*,
  size_t*);
void FC_FUNC_(bind_f_diff_i0i1, BIND_F_DIFF_I0I1)(const size_t*,
  int*,
  const int*,
  const size_t*,
  int*);
void FC_FUNC_(bind_f_sizeof_i1, BIND_F_SIZEOF_I1)(size_t*,
  const int*,
  const size_t*);
void FC_FUNC_(bind_f_sizeof_i2, BIND_F_SIZEOF_I2)(size_t*,
  const int*,
  const size_t*,
  const size_t*);
void FC_FUNC_(bind_f_sizeof_i3, BIND_F_SIZEOF_I3)(size_t*,
  const int*,
  const size_t*,
  const size_t*,
  const size_t*);
void FC_FUNC_(bind_f_sizeof_i4, BIND_F_SIZEOF_I4)(size_t*,
  const int*,
  const size_t*,
  const size_t*,
  const size_t*,
  const size_t*);
void FC_FUNC_(bind_f_sizeof_i5, BIND_F_SIZEOF_I5)(size_t*,
  const int*,
  const size_t*,
  const size_t*,
  const size_t*,
  const size_t*,
  const size_t*);
void FC_FUNC_(bind_f_sizeof_li1, BIND_F_SIZEOF_LI1)(size_t*,
  const size_t*,
  const size_t*);
void FC_FUNC_(bind_f_sizeof_li2, BIND_F_SIZEOF_LI2)(size_t*,
  const size_t*,
  const size_t*,
  const size_t*);
void FC_FUNC_(bind_f_sizeof_li3, BIND_F_SIZEOF_LI3)(size_t*,
  const size_t*,
  const size_t*,
  const size_t*,
  const size_t*);
void FC_FUNC_(bind_f_sizeof_li4, BIND_F_SIZEOF_LI4)(size_t*,
  const size_t*,
  const size_t*,
  const size_t*,
  const size_t*,
  const size_t*);
void FC_FUNC_(bind_f_sizeof_li5, BIND_F_SIZEOF_LI5)(size_t*,
  const size_t*,
  const size_t*,
  const size_t*,
  const size_t*,
  const size_t*,
  const size_t*);
void FC_FUNC_(bind_f_sizeof_d1, BIND_F_SIZEOF_D1)(size_t*,
  const double*,
  const size_t*);
void FC_FUNC_(bind_f_sizeof_d2, BIND_F_SIZEOF_D2)(size_t*,
  const double*,
  const size_t*,
  const size_t*);
void FC_FUNC_(bind_f_sizeof_d3, BIND_F_SIZEOF_D3)(size_t*,
  const double*,
  const size_t*,
  const size_t*,
  const size_t*);
void FC_FUNC_(bind_f_sizeof_d4, BIND_F_SIZEOF_D4)(size_t*,
  const double*,
  const size_t*,
  const size_t*,
  const size_t*,
  const size_t*);
void FC_FUNC_(bind_f_sizeof_d5, BIND_F_SIZEOF_D5)(size_t*,
  const double*,
  const size_t*,
  const size_t*,
  const size_t*,
  const size_t*,
  const size_t*);
void FC_FUNC_(bind_f_sizeof_d6, BIND_F_SIZEOF_D6)(size_t*,
  const double*,
  const size_t*,
  const size_t*,
  const size_t*,
  const size_t*,
  const size_t*,
  const size_t*);
void FC_FUNC_(bind_f_sizeof_d7, BIND_F_SIZEOF_D7)(size_t*,
  const double*,
  const size_t*,
  const size_t*,
  const size_t*,
  const size_t*,
  const size_t*,
  const size_t*,
  const size_t*);
void FC_FUNC_(bind_f_sizeof_r1, BIND_F_SIZEOF_R1)(size_t*,
  const float*,
  const size_t*);
void FC_FUNC_(bind_f_sizeof_r2, BIND_F_SIZEOF_R2)(size_t*,
  const float*,
  const size_t*,
  const size_t*);
void FC_FUNC_(bind_f_sizeof_r3, BIND_F_SIZEOF_R3)(size_t*,
  const float*,
  const size_t*,
  const size_t*,
  const size_t*);
void FC_FUNC_(bind_f_sizeof_r4, BIND_F_SIZEOF_R4)(size_t*,
  const float*,
  const size_t*,
  const size_t*,
  const size_t*,
  const size_t*);
void FC_FUNC_(bind_f_sizeof_r5, BIND_F_SIZEOF_R5)(size_t*,
  const float*,
  const size_t*,
  const size_t*,
  const size_t*,
  const size_t*,
  const size_t*);
void FC_FUNC_(bind_f_sizeof_c0, BIND_F_SIZEOF_C0)(size_t*,
  const char*,
  const size_t*,
  size_t);
void FC_FUNC_(bind_f_size_i0, BIND_F_SIZE_I0)(size_t*,
  const int*);
void FC_FUNC_(bind_f_size_i1, BIND_F_SIZE_I1)(size_t*,
  const int*,
  const size_t*);
void FC_FUNC_(bind_f_size_i2, BIND_F_SIZE_I2)(size_t*,
  const int*,
  const size_t*,
  const size_t*);
void FC_FUNC_(bind_f_size_i3, BIND_F_SIZE_I3)(size_t*,
  const int*,
  const size_t*,
  const size_t*,
  const size_t*);
void FC_FUNC_(bind_f_size_i4, BIND_F_SIZE_I4)(size_t*,
  const int*,
  const size_t*,
  const size_t*,
  const size_t*,
  const size_t*);
void FC_FUNC_(bind_f_size_i5, BIND_F_SIZE_I5)(size_t*,
  const int*,
  const size_t*,
  const size_t*,
  const size_t*,
  const size_t*,
  const size_t*);
void FC_FUNC_(bind_f_size_li1, BIND_F_SIZE_LI1)(size_t*,
  const size_t*,
  const size_t*);
void FC_FUNC_(bind_f_size_li2, BIND_F_SIZE_LI2)(size_t*,
  const size_t*,
  const size_t*,
  const size_t*);
void FC_FUNC_(bind_f_size_li3, BIND_F_SIZE_LI3)(size_t*,
  const size_t*,
  const size_t*,
  const size_t*,
  const size_t*);
void FC_FUNC_(bind_f_size_li4, BIND_F_SIZE_LI4)(size_t*,
  const size_t*,
  const size_t*,
  const size_t*,
  const size_t*,
  const size_t*);
void FC_FUNC_(bind_f_size_li5, BIND_F_SIZE_LI5)(size_t*,
  const size_t*,
  const size_t*,
  const size_t*,
  const size_t*,
  const size_t*,
  const size_t*);
void FC_FUNC_(bind_f_size_d0, BIND_F_SIZE_D0)(size_t*,
  const double*);
void FC_FUNC_(bind_f_size_d1, BIND_F_SIZE_D1)(size_t*,
  const double*,
  const size_t*);
void FC_FUNC_(bind_f_size_d2, BIND_F_SIZE_D2)(size_t*,
  const double*,
  const size_t*,
  const size_t*);
void FC_FUNC_(bind_f_size_d3, BIND_F_SIZE_D3)(size_t*,
  const double*,
  const size_t*,
  const size_t*,
  const size_t*);
void FC_FUNC_(bind_f_size_d4, BIND_F_SIZE_D4)(size_t*,
  const double*,
  const size_t*,
  const size_t*,
  const size_t*,
  const size_t*);
void FC_FUNC_(bind_f_size_d5, BIND_F_SIZE_D5)(size_t*,
  const double*,
  const size_t*,
  const size_t*,
  const size_t*,
  const size_t*,
  const size_t*);
void FC_FUNC_(bind_f_size_d6, BIND_F_SIZE_D6)(size_t*,
  const double*,
  const size_t*,
  const size_t*,
  const size_t*,
  const size_t*,
  const size_t*,
  const size_t*);
void FC_FUNC_(bind_f_size_d7, BIND_F_SIZE_D7)(size_t*,
  const double*,
  const size_t*,
  const size_t*,
  const size_t*,
  const size_t*,
  const size_t*,
  const size_t*,
  const size_t*);
void FC_FUNC_(bind_f_size_r1, BIND_F_SIZE_R1)(size_t*,
  const float*,
  const size_t*);
void FC_FUNC_(bind_f_size_r2, BIND_F_SIZE_R2)(size_t*,
  const float*,
  const size_t*,
  const size_t*);
void FC_FUNC_(bind_f_size_r3, BIND_F_SIZE_R3)(size_t*,
  const float*,
  const size_t*,
  const size_t*,
  const size_t*);
void FC_FUNC_(bind_f_size_r4, BIND_F_SIZE_R4)(size_t*,
  const float*,
  const size_t*,
  const size_t*,
  const size_t*,
  const size_t*);
void FC_FUNC_(bind_f_size_r5, BIND_F_SIZE_R5)(size_t*,
  const float*,
  const size_t*,
  const size_t*,
  const size_t*,
  const size_t*,
  const size_t*);
void FC_FUNC_(bind_f_size_c0, BIND_F_SIZE_C0)(size_t*,
  const char*,
  const size_t*,
  size_t);
void FC_FUNC_(bind_zero_string, BIND_ZERO_STRING)(char*,
  const size_t*,
  size_t);
void FC_FUNC_(bind_zero_li, BIND_ZERO_LI)(size_t*);
void FC_FUNC_(bind_zero_i, BIND_ZERO_I)(int*);
void FC_FUNC_(bind_zero_r, BIND_ZERO_R)(float*);
void FC_FUNC_(bind_zero_d, BIND_ZERO_D)(double*);
void FC_FUNC_(bind_zero_l, BIND_ZERO_L)(int*);
void FC_FUNC_(bind_put_to_zero_r1, BIND_PUT_TO_ZERO_R1)(float*,
  const size_t*);
void FC_FUNC_(bind_put_to_zero_double, BIND_PUT_TO_ZERO_DOUBLE)(const int*,
  double*);
void FC_FUNC_(bind_put_to_zero_double_1, BIND_PUT_TO_ZERO_DOUBLE_1)(double*,
  const size_t*);
void FC_FUNC_(bind_put_to_zero_double_2, BIND_PUT_TO_ZERO_DOUBLE_2)(double*,
  const size_t*,
  const size_t*);
void FC_FUNC_(bind_put_to_zero_double_3, BIND_PUT_TO_ZERO_DOUBLE_3)(double*,
  const size_t*,
  const size_t*,
  const size_t*);
void FC_FUNC_(bind_put_to_zero_double_4, BIND_PUT_TO_ZERO_DOUBLE_4)(double*,
  const size_t*,
  const size_t*,
  const size_t*,
  const size_t*);
void FC_FUNC_(bind_put_to_zero_double_5, BIND_PUT_TO_ZERO_DOUBLE_5)(double*,
  const size_t*,
  const size_t*,
  const size_t*,
  const size_t*,
  const size_t*);
void FC_FUNC_(bind_put_to_zero_double_6, BIND_PUT_TO_ZERO_DOUBLE_6)(double*,
  const size_t*,
  const size_t*,
  const size_t*,
  const size_t*,
  const size_t*,
  const size_t*);
void FC_FUNC_(bind_put_to_zero_double_7, BIND_PUT_TO_ZERO_DOUBLE_7)(double*,
  const size_t*,
  const size_t*,
  const size_t*,
  const size_t*,
  const size_t*,
  const size_t*,
  const size_t*);
void FC_FUNC_(bind_put_to_zero_integer, BIND_PUT_TO_ZERO_INTEGER)(const int*,
  int*);
void FC_FUNC_(bind_put_to_zero_integer1, BIND_PUT_TO_ZERO_INTEGER1)(int*,
  const size_t*);
void FC_FUNC_(bind_put_to_zero_integer2, BIND_PUT_TO_ZERO_INTEGER2)(int*,
  const size_t*,
  const size_t*);
void FC_FUNC_(bind_put_to_zero_integer3, BIND_PUT_TO_ZERO_INTEGER3)(int*,
  const size_t*,
  const size_t*,
  const size_t*);
void FC_FUNC_(bind_put_to_zero_long, BIND_PUT_TO_ZERO_LONG)(const int*,
  size_t*);
void FC_FUNC_(bind_put_to_zero_long1, BIND_PUT_TO_ZERO_LONG1)(size_t*,
  const size_t*);
void FC_FUNC_(bind_put_to_zero_long2, BIND_PUT_TO_ZERO_LONG2)(size_t*,
  const size_t*,
  const size_t*);
void FC_FUNC_(bind_put_to_zero_long3, BIND_PUT_TO_ZERO_LONG3)(size_t*,
  const size_t*,
  const size_t*,
  const size_t*);
void FC_FUNC_(bind_f_inc_i0, BIND_F_INC_I0)(int*,
  const int*);
void FC_FUNC_(bind_f_ht_long, BIND_F_HT_LONG)(char*,
  const size_t*,
  const int*,
  size_t);
void FC_FUNC_(bind_f_humantime, BIND_F_HUMANTIME)(char*,
  const double*,
  const int*,
  size_t);
void FC_FUNC_(bind_f_assert, BIND_F_ASSERT)(const int*,
  const char*,
  const size_t*,
  const int*,
  const char*,
  const size_t*,
  size_t,
  size_t);
void FC_FUNC_(bind_f_assert_str, BIND_F_ASSERT_STR)(const int*,
  const FString::f90_f_string*,
  const int*,
  const char*,
  const size_t*,
  size_t);
void FC_FUNC_(bind_f_assert_double, BIND_F_ASSERT_DOUBLE)(const double*,
  const char*,
  const size_t*,
  const int*,
  const char*,
  const size_t*,
  const double*,
  size_t,
  size_t);
void FC_FUNC_(bind_f_savetxt_d2, BIND_F_SAVETXT_D2)(const char*,
  const size_t*,
  const double*,
  const size_t*,
  const size_t*,
  size_t);
void FC_FUNC_(bind_f_get_option_l, BIND_F_GET_OPTION_L)(int*,
  const int*,
  const int*);
void FC_FUNC_(bind_f_null_i0, BIND_F_NULL_I0)(int*,
  const FNoneObject::f90_f_none_object*);
void FC_FUNC_(bind_f_null_r0, BIND_F_NULL_R0)(float*,
  const FNoneObject::f90_f_none_object*);
void FC_FUNC_(bind_f_null_d0, BIND_F_NULL_D0)(double*,
  const FNoneObject::f90_f_none_object*);
void FC_FUNC_(bind_f_null_l0, BIND_F_NULL_L0)(int*,
  const FNoneObject::f90_f_none_object*);
}

size_t Futile::f_time(void)
{
  size_t out_f_time;
  FC_FUNC_(bind_f_time, BIND_F_TIME)
    (&out_f_time);
  return out_f_time;
}

bool Futile::f_tty(int unit)
{
  int out_f_tty;
  FC_FUNC_(bind_f_tty, BIND_F_TTY)
    (&out_f_tty, &unit);
  return out_f_tty;
}

int Futile::f_get_free_unit(const int (*unit))
{
  int out_unt2;
  FC_FUNC_(bind_f_get_free_unit, BIND_F_GET_FREE_UNIT)
    (&out_unt2, unit);
  return out_unt2;
}

int Futile::f_getpid(void)
{
  int out_f_getpid;
  FC_FUNC_(bind_f_getpid, BIND_F_GETPID)
    (&out_f_getpid);
  return out_f_getpid;
}

void Futile::f_utils_errors(void)
{
  FC_FUNC_(bind_f_utils_errors, BIND_F_UTILS_ERRORS)
    ();
}

void Futile::f_pause(int sec)
{
  FC_FUNC_(bind_f_pause, BIND_F_PAUSE)
    (&sec);
}

void Futile::f_utils_recl(int unt,
    int recl_max,
    int& recl)
{
  FC_FUNC_(bind_f_utils_recl, BIND_F_UTILS_RECL)
    (&unt, &recl_max, &recl);
}

void Futile::f_file_exists(const char* file,
    bool& exists)
{
  size_t file_chk_len, file_len = file_chk_len = file ? strlen(file) : 0;
  int exists_conv;
  FC_FUNC_(bind_f_file_exists, BIND_F_FILE_EXISTS)
    (file, &file_len, &exists_conv, file_chk_len);
  exists = exists_conv;
}

void Futile::f_close(int unit)
{
  FC_FUNC_(bind_f_close, BIND_F_CLOSE)
    (&unit);
}

void Futile::f_file_unit(const char* file,
    int& unit)
{
  size_t file_chk_len, file_len = file_chk_len = file ? strlen(file) : 0;
  FC_FUNC_(bind_f_file_unit, BIND_F_FILE_UNIT)
    (file, &file_len, &unit, file_chk_len);
}

void Futile::f_mkdir(const char* dir,
    char* path,
    size_t path_len)
{
  size_t dir_chk_len, dir_len = dir_chk_len = dir ? strlen(dir) : 0;
  size_t path_chk_len = path_len;
  FC_FUNC_(bind_f_mkdir, BIND_F_MKDIR)
    (dir, &dir_len, path, &path_len, dir_chk_len, path_chk_len);
}

void Futile::f_delete_file(const char* file)
{
  size_t file_chk_len, file_len = file_chk_len = file ? strlen(file) : 0;
  FC_FUNC_(bind_f_delete_file, BIND_F_DELETE_FILE)
    (file, &file_len, file_chk_len);
}

void Futile::f_move_file(const char* src,
    const char* dest)
{
  size_t src_chk_len, src_len = src_chk_len = src ? strlen(src) : 0;
  size_t dest_chk_len, dest_len = dest_chk_len = dest ? strlen(dest) : 0;
  FC_FUNC_(bind_f_move_file, BIND_F_MOVE_FILE)
    (src, &src_len, dest, &dest_len, src_chk_len, dest_chk_len);
}

void Futile::f_system(const char* command)
{
  size_t command_chk_len, command_len = command_chk_len = command ? strlen(command) : 0;
  FC_FUNC_(bind_f_system, BIND_F_SYSTEM)
    (command, &command_len, command_chk_len);
}

void Futile::f_rewind(int unit)
{
  FC_FUNC_(bind_f_rewind, BIND_F_REWIND)
    (&unit);
}

void Futile::f_open_file(int& unit,
    const char* file,
    const char (*status),
    const char (*position),
    const char (*action),
    const bool (*binary))
{
  size_t file_chk_len, file_len = file_chk_len = file ? strlen(file) : 0;
  size_t status_chk_len, status_len = status_chk_len = status ? strlen(status) : 0;
  size_t position_chk_len, position_len = position_chk_len = position ? strlen(position) : 0;
  size_t action_chk_len, action_len = action_chk_len = action ? strlen(action) : 0;
  int binary_conv = binary ? *binary : 0;
  FC_FUNC_(bind_f_open_file, BIND_F_OPEN_FILE)
    (&unit, file, &file_len, status, &status_len, position, &position_len, action, &action_len,  binary ? &binary_conv : NULL, file_chk_len, status_chk_len, position_chk_len, action_chk_len);
}

void Futile::f_diff(size_t n,
    int* a_add,
    int* b_add,
    int& diff)
{
  FC_FUNC_(bind_f_diff_i, BIND_F_DIFF_I)
    (&n, a_add, b_add, &diff);
}

void Futile::f_diff(size_t n,
    const int* a,
    size_t a_dim_0,
    size_t a_dim_1,
    const int* b,
    size_t b_dim_0,
    int& diff)
{
  FC_FUNC_(bind_f_diff_i2i1, BIND_F_DIFF_I2I1)
    (&n, a, &a_dim_0, &a_dim_1, b, &b_dim_0, &diff);
}

void Futile::f_diff(size_t n,
    const int* a,
    size_t a_dim_0,
    size_t a_dim_1,
    size_t a_dim_2,
    const int* b,
    size_t b_dim_0,
    int& diff)
{
  FC_FUNC_(bind_f_diff_i3i1, BIND_F_DIFF_I3I1)
    (&n, a, &a_dim_0, &a_dim_1, &a_dim_2, b, &b_dim_0, &diff);
}

void Futile::f_diff(size_t n,
    const int* a,
    size_t a_dim_0,
    size_t a_dim_1,
    const int* b,
    size_t b_dim_0,
    size_t b_dim_1,
    int& diff)
{
  FC_FUNC_(bind_f_diff_i2, BIND_F_DIFF_I2)
    (&n, a, &a_dim_0, &a_dim_1, b, &b_dim_0, &b_dim_1, &diff);
}

void Futile::f_diff(size_t n,
    const int* a,
    size_t a_dim_0,
    const int* b,
    size_t b_dim_0,
    int& diff)
{
  FC_FUNC_(bind_f_diff_i1, BIND_F_DIFF_I1)
    (&n, a, &a_dim_0, b, &b_dim_0, &diff);
}

void Futile::f_diff(size_t n,
    const int* a,
    size_t a_dim_0,
    const int* b,
    size_t b_dim_0,
    size_t b_dim_1,
    int& diff)
{
  FC_FUNC_(bind_f_diff_i1i2, BIND_F_DIFF_I1I2)
    (&n, a, &a_dim_0, b, &b_dim_0, &b_dim_1, &diff);
}

void Futile::f_diff(size_t n,
    long* a_add,
    long* b_add,
    long& diff)
{
  FC_FUNC_(bind_f_diff_li, BIND_F_DIFF_LI)
    (&n, a_add, b_add, &diff);
}

void Futile::f_diff(size_t n,
    const long* a,
    size_t a_dim_0,
    size_t a_dim_1,
    const long* b,
    size_t b_dim_0,
    long& diff)
{
  FC_FUNC_(bind_f_diff_li2li1, BIND_F_DIFF_LI2LI1)
    (&n, a, &a_dim_0, &a_dim_1, b, &b_dim_0, &diff);
}

void Futile::f_diff(size_t n,
    const long* a,
    size_t a_dim_0,
    size_t a_dim_1,
    const long* b,
    size_t b_dim_0,
    size_t b_dim_1,
    long& diff)
{
  FC_FUNC_(bind_f_diff_li2, BIND_F_DIFF_LI2)
    (&n, a, &a_dim_0, &a_dim_1, b, &b_dim_0, &b_dim_1, &diff);
}

void Futile::f_diff(size_t n,
    const long* a,
    size_t a_dim_0,
    const long* b,
    size_t b_dim_0,
    long& diff)
{
  FC_FUNC_(bind_f_diff_li1, BIND_F_DIFF_LI1)
    (&n, a, &a_dim_0, b, &b_dim_0, &diff);
}

void Futile::f_diff(size_t n,
    const long* a,
    size_t a_dim_0,
    const long* b,
    size_t b_dim_0,
    size_t b_dim_1,
    long& diff)
{
  FC_FUNC_(bind_f_diff_li1li2, BIND_F_DIFF_LI1LI2)
    (&n, a, &a_dim_0, b, &b_dim_0, &b_dim_1, &diff);
}

void Futile::f_diff(size_t n,
    float* a_add,
    float* b_add,
    float& diff)
{
  FC_FUNC_(bind_f_diff_r, BIND_F_DIFF_R)
    (&n, a_add, b_add, &diff);
}

void Futile::f_diff(size_t n,
    double* a_add,
    double* b_add,
    double& diff)
{
  FC_FUNC_(bind_f_diff_d, BIND_F_DIFF_D)
    (&n, a_add, b_add, &diff);
}

void Futile::f_diff(size_t n,
    const double* a,
    size_t a_dim_0,
    const double* b,
    size_t b_dim_0,
    double& diff,
    size_t (*ind))
{
  FC_FUNC_(bind_f_diff_d1, BIND_F_DIFF_D1)
    (&n, a, &a_dim_0, b, &b_dim_0, &diff, ind);
}

void Futile::f_diff(size_t n,
    const double* a,
    size_t a_dim_0,
    size_t a_dim_1,
    const double* b,
    size_t b_dim_0,
    size_t b_dim_1,
    size_t b_dim_2,
    double& diff)
{
  FC_FUNC_(bind_f_diff_d2d3, BIND_F_DIFF_D2D3)
    (&n, a, &a_dim_0, &a_dim_1, b, &b_dim_0, &b_dim_1, &b_dim_2, &diff);
}

void Futile::f_diff(size_t n,
    const double* a,
    size_t a_dim_0,
    size_t a_dim_1,
    const double* b,
    size_t b_dim_0,
    double& diff)
{
  FC_FUNC_(bind_f_diff_d2d1, BIND_F_DIFF_D2D1)
    (&n, a, &a_dim_0, &a_dim_1, b, &b_dim_0, &diff);
}

void Futile::f_diff(size_t n,
    const double* a,
    size_t a_dim_0,
    size_t a_dim_1,
    size_t a_dim_2,
    const double* b,
    size_t b_dim_0,
    double& diff)
{
  FC_FUNC_(bind_f_diff_d3d1, BIND_F_DIFF_D3D1)
    (&n, a, &a_dim_0, &a_dim_1, &a_dim_2, b, &b_dim_0, &diff);
}

void Futile::f_diff(size_t n,
    double& a,
    const double* b,
    size_t b_dim_0,
    double& diff)
{
  FC_FUNC_(bind_f_diff_d0d1, BIND_F_DIFF_D0D1)
    (&n, &a, b, &b_dim_0, &diff);
}

void Futile::f_diff(size_t n,
    const double* a,
    size_t a_dim_0,
    size_t a_dim_1,
    const double* b,
    size_t b_dim_0,
    size_t b_dim_1,
    double& diff)
{
  FC_FUNC_(bind_f_diff_d2, BIND_F_DIFF_D2)
    (&n, a, &a_dim_0, &a_dim_1, b, &b_dim_0, &b_dim_1, &diff);
}

void Futile::f_diff(size_t n,
    const double* a,
    size_t a_dim_0,
    size_t a_dim_1,
    size_t a_dim_2,
    const double* b,
    size_t b_dim_0,
    size_t b_dim_1,
    size_t b_dim_2,
    double& diff)
{
  FC_FUNC_(bind_f_diff_d3, BIND_F_DIFF_D3)
    (&n, a, &a_dim_0, &a_dim_1, &a_dim_2, b, &b_dim_0, &b_dim_1, &b_dim_2, &diff);
}

void Futile::f_diff(size_t n,
    const double* a,
    size_t a_dim_0,
    const double* b,
    size_t b_dim_0,
    size_t b_dim_1,
    double& diff)
{
  FC_FUNC_(bind_f_diff_d1d2, BIND_F_DIFF_D1D2)
    (&n, a, &a_dim_0, b, &b_dim_0, &b_dim_1, &diff);
}

void Futile::f_diff(size_t n,
    const char* a,
    const int* b,
    size_t b_dim_0,
    int& diff)
{
  size_t a_chk_len, a_len = a_chk_len = a ? strlen(a) : 0;
  FC_FUNC_(bind_f_diff_c1i1, BIND_F_DIFF_C1I1)
    (&n, a, &a_len, b, &b_dim_0, &diff, a_chk_len);
}

void Futile::f_diff(size_t n,
    const char* a,
    const size_t* b,
    size_t b_dim_0,
    size_t& diff)
{
  size_t a_chk_len, a_len = a_chk_len = a ? strlen(a) : 0;
  FC_FUNC_(bind_f_diff_c1li1, BIND_F_DIFF_C1LI1)
    (&n, a, &a_len, b, &b_dim_0, &diff, a_chk_len);
}

void Futile::f_diff(size_t n,
    size_t& a,
    const size_t* b,
    size_t b_dim_0,
    size_t& diff)
{
  FC_FUNC_(bind_f_diff_li0li1, BIND_F_DIFF_LI0LI1)
    (&n, &a, b, &b_dim_0, &diff);
}

void Futile::f_diff(size_t n,
    int& a,
    const int* b,
    size_t b_dim_0,
    int& diff)
{
  FC_FUNC_(bind_f_diff_i0i1, BIND_F_DIFF_I0I1)
    (&n, &a, b, &b_dim_0, &diff);
}

size_t Futile::f_sizeof(const int* datatype,
    size_t datatype_dim_0)
{
  size_t out_s;
  FC_FUNC_(bind_f_sizeof_i1, BIND_F_SIZEOF_I1)
    (&out_s, datatype, &datatype_dim_0);
  return out_s;
}

size_t Futile::f_sizeof(const int* datatype,
    size_t datatype_dim_0,
    size_t datatype_dim_1)
{
  size_t out_s;
  FC_FUNC_(bind_f_sizeof_i2, BIND_F_SIZEOF_I2)
    (&out_s, datatype, &datatype_dim_0, &datatype_dim_1);
  return out_s;
}

size_t Futile::f_sizeof(const int* datatype,
    size_t datatype_dim_0,
    size_t datatype_dim_1,
    size_t datatype_dim_2)
{
  size_t out_s;
  FC_FUNC_(bind_f_sizeof_i3, BIND_F_SIZEOF_I3)
    (&out_s, datatype, &datatype_dim_0, &datatype_dim_1, &datatype_dim_2);
  return out_s;
}

size_t Futile::f_sizeof(const int* datatype,
    size_t datatype_dim_0,
    size_t datatype_dim_1,
    size_t datatype_dim_2,
    size_t datatype_dim_3)
{
  size_t out_s;
  FC_FUNC_(bind_f_sizeof_i4, BIND_F_SIZEOF_I4)
    (&out_s, datatype, &datatype_dim_0, &datatype_dim_1, &datatype_dim_2, &datatype_dim_3);
  return out_s;
}

size_t Futile::f_sizeof(const int* datatype,
    size_t datatype_dim_0,
    size_t datatype_dim_1,
    size_t datatype_dim_2,
    size_t datatype_dim_3,
    size_t datatype_dim_4)
{
  size_t out_s;
  FC_FUNC_(bind_f_sizeof_i5, BIND_F_SIZEOF_I5)
    (&out_s, datatype, &datatype_dim_0, &datatype_dim_1, &datatype_dim_2, &datatype_dim_3, &datatype_dim_4);
  return out_s;
}

size_t Futile::f_sizeof(const size_t* datatype,
    size_t datatype_dim_0)
{
  size_t out_s;
  FC_FUNC_(bind_f_sizeof_li1, BIND_F_SIZEOF_LI1)
    (&out_s, datatype, &datatype_dim_0);
  return out_s;
}

size_t Futile::f_sizeof(const size_t* datatype,
    size_t datatype_dim_0,
    size_t datatype_dim_1)
{
  size_t out_s;
  FC_FUNC_(bind_f_sizeof_li2, BIND_F_SIZEOF_LI2)
    (&out_s, datatype, &datatype_dim_0, &datatype_dim_1);
  return out_s;
}

size_t Futile::f_sizeof(const size_t* datatype,
    size_t datatype_dim_0,
    size_t datatype_dim_1,
    size_t datatype_dim_2)
{
  size_t out_s;
  FC_FUNC_(bind_f_sizeof_li3, BIND_F_SIZEOF_LI3)
    (&out_s, datatype, &datatype_dim_0, &datatype_dim_1, &datatype_dim_2);
  return out_s;
}

size_t Futile::f_sizeof(const size_t* datatype,
    size_t datatype_dim_0,
    size_t datatype_dim_1,
    size_t datatype_dim_2,
    size_t datatype_dim_3)
{
  size_t out_s;
  FC_FUNC_(bind_f_sizeof_li4, BIND_F_SIZEOF_LI4)
    (&out_s, datatype, &datatype_dim_0, &datatype_dim_1, &datatype_dim_2, &datatype_dim_3);
  return out_s;
}

size_t Futile::f_sizeof(const size_t* datatype,
    size_t datatype_dim_0,
    size_t datatype_dim_1,
    size_t datatype_dim_2,
    size_t datatype_dim_3,
    size_t datatype_dim_4)
{
  size_t out_s;
  FC_FUNC_(bind_f_sizeof_li5, BIND_F_SIZEOF_LI5)
    (&out_s, datatype, &datatype_dim_0, &datatype_dim_1, &datatype_dim_2, &datatype_dim_3, &datatype_dim_4);
  return out_s;
}

size_t Futile::f_sizeof(const double* datatype,
    size_t datatype_dim_0)
{
  size_t out_s;
  FC_FUNC_(bind_f_sizeof_d1, BIND_F_SIZEOF_D1)
    (&out_s, datatype, &datatype_dim_0);
  return out_s;
}

size_t Futile::f_sizeof(const double* datatype,
    size_t datatype_dim_0,
    size_t datatype_dim_1)
{
  size_t out_s;
  FC_FUNC_(bind_f_sizeof_d2, BIND_F_SIZEOF_D2)
    (&out_s, datatype, &datatype_dim_0, &datatype_dim_1);
  return out_s;
}

size_t Futile::f_sizeof(const double* datatype,
    size_t datatype_dim_0,
    size_t datatype_dim_1,
    size_t datatype_dim_2)
{
  size_t out_s;
  FC_FUNC_(bind_f_sizeof_d3, BIND_F_SIZEOF_D3)
    (&out_s, datatype, &datatype_dim_0, &datatype_dim_1, &datatype_dim_2);
  return out_s;
}

size_t Futile::f_sizeof(const double* datatype,
    size_t datatype_dim_0,
    size_t datatype_dim_1,
    size_t datatype_dim_2,
    size_t datatype_dim_3)
{
  size_t out_s;
  FC_FUNC_(bind_f_sizeof_d4, BIND_F_SIZEOF_D4)
    (&out_s, datatype, &datatype_dim_0, &datatype_dim_1, &datatype_dim_2, &datatype_dim_3);
  return out_s;
}

size_t Futile::f_sizeof(const double* datatype,
    size_t datatype_dim_0,
    size_t datatype_dim_1,
    size_t datatype_dim_2,
    size_t datatype_dim_3,
    size_t datatype_dim_4)
{
  size_t out_s;
  FC_FUNC_(bind_f_sizeof_d5, BIND_F_SIZEOF_D5)
    (&out_s, datatype, &datatype_dim_0, &datatype_dim_1, &datatype_dim_2, &datatype_dim_3, &datatype_dim_4);
  return out_s;
}

size_t Futile::f_sizeof(const double* datatype,
    size_t datatype_dim_0,
    size_t datatype_dim_1,
    size_t datatype_dim_2,
    size_t datatype_dim_3,
    size_t datatype_dim_4,
    size_t datatype_dim_5)
{
  size_t out_s;
  FC_FUNC_(bind_f_sizeof_d6, BIND_F_SIZEOF_D6)
    (&out_s, datatype, &datatype_dim_0, &datatype_dim_1, &datatype_dim_2, &datatype_dim_3, &datatype_dim_4, &datatype_dim_5);
  return out_s;
}

size_t Futile::f_sizeof(const double* datatype,
    size_t datatype_dim_0,
    size_t datatype_dim_1,
    size_t datatype_dim_2,
    size_t datatype_dim_3,
    size_t datatype_dim_4,
    size_t datatype_dim_5,
    size_t datatype_dim_6)
{
  size_t out_s;
  FC_FUNC_(bind_f_sizeof_d7, BIND_F_SIZEOF_D7)
    (&out_s, datatype, &datatype_dim_0, &datatype_dim_1, &datatype_dim_2, &datatype_dim_3, &datatype_dim_4, &datatype_dim_5, &datatype_dim_6);
  return out_s;
}

size_t Futile::f_sizeof(const float* datatype,
    size_t datatype_dim_0)
{
  size_t out_s;
  FC_FUNC_(bind_f_sizeof_r1, BIND_F_SIZEOF_R1)
    (&out_s, datatype, &datatype_dim_0);
  return out_s;
}

size_t Futile::f_sizeof(const float* datatype,
    size_t datatype_dim_0,
    size_t datatype_dim_1)
{
  size_t out_s;
  FC_FUNC_(bind_f_sizeof_r2, BIND_F_SIZEOF_R2)
    (&out_s, datatype, &datatype_dim_0, &datatype_dim_1);
  return out_s;
}

size_t Futile::f_sizeof(const float* datatype,
    size_t datatype_dim_0,
    size_t datatype_dim_1,
    size_t datatype_dim_2)
{
  size_t out_s;
  FC_FUNC_(bind_f_sizeof_r3, BIND_F_SIZEOF_R3)
    (&out_s, datatype, &datatype_dim_0, &datatype_dim_1, &datatype_dim_2);
  return out_s;
}

size_t Futile::f_sizeof(const float* datatype,
    size_t datatype_dim_0,
    size_t datatype_dim_1,
    size_t datatype_dim_2,
    size_t datatype_dim_3)
{
  size_t out_s;
  FC_FUNC_(bind_f_sizeof_r4, BIND_F_SIZEOF_R4)
    (&out_s, datatype, &datatype_dim_0, &datatype_dim_1, &datatype_dim_2, &datatype_dim_3);
  return out_s;
}

size_t Futile::f_sizeof(const float* datatype,
    size_t datatype_dim_0,
    size_t datatype_dim_1,
    size_t datatype_dim_2,
    size_t datatype_dim_3,
    size_t datatype_dim_4)
{
  size_t out_s;
  FC_FUNC_(bind_f_sizeof_r5, BIND_F_SIZEOF_R5)
    (&out_s, datatype, &datatype_dim_0, &datatype_dim_1, &datatype_dim_2, &datatype_dim_3, &datatype_dim_4);
  return out_s;
}

size_t Futile::f_sizeof(const char* datatype)
{
  size_t out_s;
  size_t datatype_chk_len, datatype_len = datatype_chk_len = datatype ? strlen(datatype) : 0;
  FC_FUNC_(bind_f_sizeof_c0, BIND_F_SIZEOF_C0)
    (&out_s, datatype, &datatype_len, datatype_chk_len);
  return out_s;
}

size_t Futile::f_size(int datatype)
{
  size_t out_s;
  FC_FUNC_(bind_f_size_i0, BIND_F_SIZE_I0)
    (&out_s, &datatype);
  return out_s;
}

size_t Futile::f_size(const int* datatype,
    size_t datatype_dim_0)
{
  size_t out_s;
  FC_FUNC_(bind_f_size_i1, BIND_F_SIZE_I1)
    (&out_s, datatype, &datatype_dim_0);
  return out_s;
}

size_t Futile::f_size(const int* datatype,
    size_t datatype_dim_0,
    size_t datatype_dim_1)
{
  size_t out_s;
  FC_FUNC_(bind_f_size_i2, BIND_F_SIZE_I2)
    (&out_s, datatype, &datatype_dim_0, &datatype_dim_1);
  return out_s;
}

size_t Futile::f_size(const int* datatype,
    size_t datatype_dim_0,
    size_t datatype_dim_1,
    size_t datatype_dim_2)
{
  size_t out_s;
  FC_FUNC_(bind_f_size_i3, BIND_F_SIZE_I3)
    (&out_s, datatype, &datatype_dim_0, &datatype_dim_1, &datatype_dim_2);
  return out_s;
}

size_t Futile::f_size(const int* datatype,
    size_t datatype_dim_0,
    size_t datatype_dim_1,
    size_t datatype_dim_2,
    size_t datatype_dim_3)
{
  size_t out_s;
  FC_FUNC_(bind_f_size_i4, BIND_F_SIZE_I4)
    (&out_s, datatype, &datatype_dim_0, &datatype_dim_1, &datatype_dim_2, &datatype_dim_3);
  return out_s;
}

size_t Futile::f_size(const int* datatype,
    size_t datatype_dim_0,
    size_t datatype_dim_1,
    size_t datatype_dim_2,
    size_t datatype_dim_3,
    size_t datatype_dim_4)
{
  size_t out_s;
  FC_FUNC_(bind_f_size_i5, BIND_F_SIZE_I5)
    (&out_s, datatype, &datatype_dim_0, &datatype_dim_1, &datatype_dim_2, &datatype_dim_3, &datatype_dim_4);
  return out_s;
}

size_t Futile::f_size(const size_t* datatype,
    size_t datatype_dim_0)
{
  size_t out_s;
  FC_FUNC_(bind_f_size_li1, BIND_F_SIZE_LI1)
    (&out_s, datatype, &datatype_dim_0);
  return out_s;
}

size_t Futile::f_size(const size_t* datatype,
    size_t datatype_dim_0,
    size_t datatype_dim_1)
{
  size_t out_s;
  FC_FUNC_(bind_f_size_li2, BIND_F_SIZE_LI2)
    (&out_s, datatype, &datatype_dim_0, &datatype_dim_1);
  return out_s;
}

size_t Futile::f_size(const size_t* datatype,
    size_t datatype_dim_0,
    size_t datatype_dim_1,
    size_t datatype_dim_2)
{
  size_t out_s;
  FC_FUNC_(bind_f_size_li3, BIND_F_SIZE_LI3)
    (&out_s, datatype, &datatype_dim_0, &datatype_dim_1, &datatype_dim_2);
  return out_s;
}

size_t Futile::f_size(const size_t* datatype,
    size_t datatype_dim_0,
    size_t datatype_dim_1,
    size_t datatype_dim_2,
    size_t datatype_dim_3)
{
  size_t out_s;
  FC_FUNC_(bind_f_size_li4, BIND_F_SIZE_LI4)
    (&out_s, datatype, &datatype_dim_0, &datatype_dim_1, &datatype_dim_2, &datatype_dim_3);
  return out_s;
}

size_t Futile::f_size(const size_t* datatype,
    size_t datatype_dim_0,
    size_t datatype_dim_1,
    size_t datatype_dim_2,
    size_t datatype_dim_3,
    size_t datatype_dim_4)
{
  size_t out_s;
  FC_FUNC_(bind_f_size_li5, BIND_F_SIZE_LI5)
    (&out_s, datatype, &datatype_dim_0, &datatype_dim_1, &datatype_dim_2, &datatype_dim_3, &datatype_dim_4);
  return out_s;
}

size_t Futile::f_size(double datatype)
{
  size_t out_s;
  FC_FUNC_(bind_f_size_d0, BIND_F_SIZE_D0)
    (&out_s, &datatype);
  return out_s;
}

size_t Futile::f_size(const double* datatype,
    size_t datatype_dim_0)
{
  size_t out_s;
  FC_FUNC_(bind_f_size_d1, BIND_F_SIZE_D1)
    (&out_s, datatype, &datatype_dim_0);
  return out_s;
}

size_t Futile::f_size(const double* datatype,
    size_t datatype_dim_0,
    size_t datatype_dim_1)
{
  size_t out_s;
  FC_FUNC_(bind_f_size_d2, BIND_F_SIZE_D2)
    (&out_s, datatype, &datatype_dim_0, &datatype_dim_1);
  return out_s;
}

size_t Futile::f_size(const double* datatype,
    size_t datatype_dim_0,
    size_t datatype_dim_1,
    size_t datatype_dim_2)
{
  size_t out_s;
  FC_FUNC_(bind_f_size_d3, BIND_F_SIZE_D3)
    (&out_s, datatype, &datatype_dim_0, &datatype_dim_1, &datatype_dim_2);
  return out_s;
}

size_t Futile::f_size(const double* datatype,
    size_t datatype_dim_0,
    size_t datatype_dim_1,
    size_t datatype_dim_2,
    size_t datatype_dim_3)
{
  size_t out_s;
  FC_FUNC_(bind_f_size_d4, BIND_F_SIZE_D4)
    (&out_s, datatype, &datatype_dim_0, &datatype_dim_1, &datatype_dim_2, &datatype_dim_3);
  return out_s;
}

size_t Futile::f_size(const double* datatype,
    size_t datatype_dim_0,
    size_t datatype_dim_1,
    size_t datatype_dim_2,
    size_t datatype_dim_3,
    size_t datatype_dim_4)
{
  size_t out_s;
  FC_FUNC_(bind_f_size_d5, BIND_F_SIZE_D5)
    (&out_s, datatype, &datatype_dim_0, &datatype_dim_1, &datatype_dim_2, &datatype_dim_3, &datatype_dim_4);
  return out_s;
}

size_t Futile::f_size(const double* datatype,
    size_t datatype_dim_0,
    size_t datatype_dim_1,
    size_t datatype_dim_2,
    size_t datatype_dim_3,
    size_t datatype_dim_4,
    size_t datatype_dim_5)
{
  size_t out_s;
  FC_FUNC_(bind_f_size_d6, BIND_F_SIZE_D6)
    (&out_s, datatype, &datatype_dim_0, &datatype_dim_1, &datatype_dim_2, &datatype_dim_3, &datatype_dim_4, &datatype_dim_5);
  return out_s;
}

size_t Futile::f_size(const double* datatype,
    size_t datatype_dim_0,
    size_t datatype_dim_1,
    size_t datatype_dim_2,
    size_t datatype_dim_3,
    size_t datatype_dim_4,
    size_t datatype_dim_5,
    size_t datatype_dim_6)
{
  size_t out_s;
  FC_FUNC_(bind_f_size_d7, BIND_F_SIZE_D7)
    (&out_s, datatype, &datatype_dim_0, &datatype_dim_1, &datatype_dim_2, &datatype_dim_3, &datatype_dim_4, &datatype_dim_5, &datatype_dim_6);
  return out_s;
}

size_t Futile::f_size(const float* datatype,
    size_t datatype_dim_0)
{
  size_t out_s;
  FC_FUNC_(bind_f_size_r1, BIND_F_SIZE_R1)
    (&out_s, datatype, &datatype_dim_0);
  return out_s;
}

size_t Futile::f_size(const float* datatype,
    size_t datatype_dim_0,
    size_t datatype_dim_1)
{
  size_t out_s;
  FC_FUNC_(bind_f_size_r2, BIND_F_SIZE_R2)
    (&out_s, datatype, &datatype_dim_0, &datatype_dim_1);
  return out_s;
}

size_t Futile::f_size(const float* datatype,
    size_t datatype_dim_0,
    size_t datatype_dim_1,
    size_t datatype_dim_2)
{
  size_t out_s;
  FC_FUNC_(bind_f_size_r3, BIND_F_SIZE_R3)
    (&out_s, datatype, &datatype_dim_0, &datatype_dim_1, &datatype_dim_2);
  return out_s;
}

size_t Futile::f_size(const float* datatype,
    size_t datatype_dim_0,
    size_t datatype_dim_1,
    size_t datatype_dim_2,
    size_t datatype_dim_3)
{
  size_t out_s;
  FC_FUNC_(bind_f_size_r4, BIND_F_SIZE_R4)
    (&out_s, datatype, &datatype_dim_0, &datatype_dim_1, &datatype_dim_2, &datatype_dim_3);
  return out_s;
}

size_t Futile::f_size(const float* datatype,
    size_t datatype_dim_0,
    size_t datatype_dim_1,
    size_t datatype_dim_2,
    size_t datatype_dim_3,
    size_t datatype_dim_4)
{
  size_t out_s;
  FC_FUNC_(bind_f_size_r5, BIND_F_SIZE_R5)
    (&out_s, datatype, &datatype_dim_0, &datatype_dim_1, &datatype_dim_2, &datatype_dim_3, &datatype_dim_4);
  return out_s;
}

size_t Futile::f_size(const char* datatype)
{
  size_t out_s;
  size_t datatype_chk_len, datatype_len = datatype_chk_len = datatype ? strlen(datatype) : 0;
  FC_FUNC_(bind_f_size_c0, BIND_F_SIZE_C0)
    (&out_s, datatype, &datatype_len, datatype_chk_len);
  return out_s;
}

void Futile::f_zero(char* str,
    size_t str_len)
{
  size_t str_chk_len = str_len;
  FC_FUNC_(bind_zero_string, BIND_ZERO_STRING)
    (str, &str_len, str_chk_len);
}

void Futile::f_zero(size_t& val)
{
  FC_FUNC_(bind_zero_li, BIND_ZERO_LI)
    (&val);
}

void Futile::f_zero(int& val)
{
  FC_FUNC_(bind_zero_i, BIND_ZERO_I)
    (&val);
}

void Futile::f_zero(float& val)
{
  FC_FUNC_(bind_zero_r, BIND_ZERO_R)
    (&val);
}

void Futile::f_zero(double& val)
{
  FC_FUNC_(bind_zero_d, BIND_ZERO_D)
    (&val);
}

void Futile::f_zero(bool& val)
{
  int val_conv;
  FC_FUNC_(bind_zero_l, BIND_ZERO_L)
    (&val_conv);
  val = val_conv;
}

void Futile::f_zero(float* da,
    size_t da_dim_0)
{
  FC_FUNC_(bind_put_to_zero_r1, BIND_PUT_TO_ZERO_R1)
    (da, &da_dim_0);
}

void Futile::f_zero(int n,
    double& da)
{
  FC_FUNC_(bind_put_to_zero_double, BIND_PUT_TO_ZERO_DOUBLE)
    (&n, &da);
}

void Futile::f_zero(double* da,
    size_t da_dim_0)
{
  FC_FUNC_(bind_put_to_zero_double_1, BIND_PUT_TO_ZERO_DOUBLE_1)
    (da, &da_dim_0);
}

void Futile::f_zero(double* da,
    size_t da_dim_0,
    size_t da_dim_1)
{
  FC_FUNC_(bind_put_to_zero_double_2, BIND_PUT_TO_ZERO_DOUBLE_2)
    (da, &da_dim_0, &da_dim_1);
}

void Futile::f_zero(double* da,
    size_t da_dim_0,
    size_t da_dim_1,
    size_t da_dim_2)
{
  FC_FUNC_(bind_put_to_zero_double_3, BIND_PUT_TO_ZERO_DOUBLE_3)
    (da, &da_dim_0, &da_dim_1, &da_dim_2);
}

void Futile::f_zero(double* da,
    size_t da_dim_0,
    size_t da_dim_1,
    size_t da_dim_2,
    size_t da_dim_3)
{
  FC_FUNC_(bind_put_to_zero_double_4, BIND_PUT_TO_ZERO_DOUBLE_4)
    (da, &da_dim_0, &da_dim_1, &da_dim_2, &da_dim_3);
}

void Futile::f_zero(double* da,
    size_t da_dim_0,
    size_t da_dim_1,
    size_t da_dim_2,
    size_t da_dim_3,
    size_t da_dim_4)
{
  FC_FUNC_(bind_put_to_zero_double_5, BIND_PUT_TO_ZERO_DOUBLE_5)
    (da, &da_dim_0, &da_dim_1, &da_dim_2, &da_dim_3, &da_dim_4);
}

void Futile::f_zero(double* da,
    size_t da_dim_0,
    size_t da_dim_1,
    size_t da_dim_2,
    size_t da_dim_3,
    size_t da_dim_4,
    size_t da_dim_5)
{
  FC_FUNC_(bind_put_to_zero_double_6, BIND_PUT_TO_ZERO_DOUBLE_6)
    (da, &da_dim_0, &da_dim_1, &da_dim_2, &da_dim_3, &da_dim_4, &da_dim_5);
}

void Futile::f_zero(double* da,
    size_t da_dim_0,
    size_t da_dim_1,
    size_t da_dim_2,
    size_t da_dim_3,
    size_t da_dim_4,
    size_t da_dim_5,
    size_t da_dim_6)
{
  FC_FUNC_(bind_put_to_zero_double_7, BIND_PUT_TO_ZERO_DOUBLE_7)
    (da, &da_dim_0, &da_dim_1, &da_dim_2, &da_dim_3, &da_dim_4, &da_dim_5, &da_dim_6);
}

void Futile::f_zero(int n,
    int& da)
{
  FC_FUNC_(bind_put_to_zero_integer, BIND_PUT_TO_ZERO_INTEGER)
    (&n, &da);
}

void Futile::f_zero(int* da,
    size_t da_dim_0)
{
  FC_FUNC_(bind_put_to_zero_integer1, BIND_PUT_TO_ZERO_INTEGER1)
    (da, &da_dim_0);
}

void Futile::f_zero(int* da,
    size_t da_dim_0,
    size_t da_dim_1)
{
  FC_FUNC_(bind_put_to_zero_integer2, BIND_PUT_TO_ZERO_INTEGER2)
    (da, &da_dim_0, &da_dim_1);
}

void Futile::f_zero(int* da,
    size_t da_dim_0,
    size_t da_dim_1,
    size_t da_dim_2)
{
  FC_FUNC_(bind_put_to_zero_integer3, BIND_PUT_TO_ZERO_INTEGER3)
    (da, &da_dim_0, &da_dim_1, &da_dim_2);
}

void Futile::f_zero(int n,
    size_t& da)
{
  FC_FUNC_(bind_put_to_zero_long, BIND_PUT_TO_ZERO_LONG)
    (&n, &da);
}

void Futile::f_zero(size_t* da,
    size_t da_dim_0)
{
  FC_FUNC_(bind_put_to_zero_long1, BIND_PUT_TO_ZERO_LONG1)
    (da, &da_dim_0);
}

void Futile::f_zero(size_t* da,
    size_t da_dim_0,
    size_t da_dim_1)
{
  FC_FUNC_(bind_put_to_zero_long2, BIND_PUT_TO_ZERO_LONG2)
    (da, &da_dim_0, &da_dim_1);
}

void Futile::f_zero(size_t* da,
    size_t da_dim_0,
    size_t da_dim_1,
    size_t da_dim_2)
{
  FC_FUNC_(bind_put_to_zero_long3, BIND_PUT_TO_ZERO_LONG3)
    (da, &da_dim_0, &da_dim_1, &da_dim_2);
}

void Futile::f_increment(int& i,
    const int (*inc))
{
  FC_FUNC_(bind_f_inc_i0, BIND_F_INC_I0)
    (&i, inc);
}

void Futile::f_humantime(char out_time[95],
    size_t ns,
    const bool (*short_bn))
{
  size_t out_time_chk_len = 95;
  int short_bn_conv = short_bn ? *short_bn : 0;
  FC_FUNC_(bind_f_ht_long, BIND_F_HT_LONG)
    (out_time, &ns,  short_bn ? &short_bn_conv : NULL, out_time_chk_len);
}

void Futile::f_humantime(char out_time[95],
    double ns,
    const bool (*short_bn))
{
  size_t out_time_chk_len = 95;
  int short_bn_conv = short_bn ? *short_bn : 0;
  FC_FUNC_(bind_f_humantime, BIND_F_HUMANTIME)
    (out_time, &ns,  short_bn ? &short_bn_conv : NULL, out_time_chk_len);
}

void Futile::f_assert(bool condition,
    const char* id,
    const int (*err_id),
    const char (*err_name))
{
  int condition_conv = condition;
  size_t id_chk_len, id_len = id_chk_len = id ? strlen(id) : 0;
  size_t err_name_chk_len, err_name_len = err_name_chk_len = err_name ? strlen(err_name) : 0;
  FC_FUNC_(bind_f_assert, BIND_F_ASSERT)
    (&condition_conv, id, &id_len, err_id, err_name, &err_name_len, id_chk_len, err_name_chk_len);
}

void Futile::f_assert(bool condition,
    const FString& id,
    const int (*err_id),
    const char (*err_name))
{
  int condition_conv = condition;
  size_t err_name_chk_len, err_name_len = err_name_chk_len = err_name ? strlen(err_name) : 0;
  FC_FUNC_(bind_f_assert_str, BIND_F_ASSERT_STR)
    (&condition_conv, id, err_id, err_name, &err_name_len, err_name_chk_len);
}

void Futile::f_assert(double condition,
    const char* id,
    const int (*err_id),
    const char (*err_name),
    const double (*tol))
{
  size_t id_chk_len, id_len = id_chk_len = id ? strlen(id) : 0;
  size_t err_name_chk_len, err_name_len = err_name_chk_len = err_name ? strlen(err_name) : 0;
  FC_FUNC_(bind_f_assert_double, BIND_F_ASSERT_DOUBLE)
    (&condition, id, &id_len, err_id, err_name, &err_name_len, tol, id_chk_len, err_name_chk_len);
}

void Futile::f_savetxt(const char* file,
    const double* data,
    size_t data_dim_0,
    size_t data_dim_1)
{
  size_t file_chk_len, file_len = file_chk_len = file ? strlen(file) : 0;
  FC_FUNC_(bind_f_savetxt_d2, BIND_F_SAVETXT_D2)
    (file, &file_len, data, &data_dim_0, &data_dim_1, file_chk_len);
}

bool Futile::f_get_option(bool default_bn,
    const bool (*opt))
{
  int out_val;
  int default_bn_conv = default_bn;
  int opt_conv = opt ? *opt : 0;
  FC_FUNC_(bind_f_get_option_l, BIND_F_GET_OPTION_L)
    (&out_val, &default_bn_conv,  opt ? &opt_conv : NULL);
  return out_val;
}

void Futile::assignment(int& val,
    const FNoneObject& nl)
{
  FC_FUNC_(bind_f_null_i0, BIND_F_NULL_I0)
    (&val, nl);
}

void Futile::assignment(float& val,
    const FNoneObject& nl)
{
  FC_FUNC_(bind_f_null_r0, BIND_F_NULL_R0)
    (&val, nl);
}

void Futile::assignment(double& val,
    const FNoneObject& nl)
{
  FC_FUNC_(bind_f_null_d0, BIND_F_NULL_D0)
    (&val, nl);
}

void Futile::assignment(bool& val,
    const FNoneObject& nl)
{
  int val_conv;
  FC_FUNC_(bind_f_null_l0, BIND_F_NULL_L0)
    (&val_conv, nl);
  val = val_conv;
}

FDumpBuffer::FDumpBuffer(const FDumpBuffer& other)
{
  FC_FUNC_(bind_f90_f_dump_buffer_copy_constructor, BIND_F90_F_DUMP_BUFFER_COPY_CONSTRUCTOR)
    (*this, other);
}

FDumpBuffer::FDumpBuffer(const char* buf,
    int ipos)
{
  size_t buf_chk_len, buf_len = buf_chk_len = buf ? strlen(buf) : 0;
  FC_FUNC_(bind_f90_f_dump_buffer_type_new, BIND_F90_F_DUMP_BUFFER_TYPE_NEW)
    (*this, buf, &buf_len, &ipos, buf_chk_len);
}

FDumpBuffer::~FDumpBuffer(void)
{
  FC_FUNC_(bind_f90_f_dump_buffer_free, BIND_F90_F_DUMP_BUFFER_FREE)
    (*this);
}

FDumpBuffer::FDumpBuffer(void)
{
  FC_FUNC_(bind_f90_f_dump_buffer_empty, BIND_F90_F_DUMP_BUFFER_EMPTY)
    (*this);
}

FNoneObject::FNoneObject(const FNoneObject& other)
{
  FC_FUNC_(bind_f90_f_none_object_copy_constructor, BIND_F90_F_NONE_OBJECT_COPY_CONSTRUCTOR)
    (*this, other);
}

FNoneObject::FNoneObject(const char* none)
{
  size_t none_chk_len, none_len = none_chk_len = none ? strlen(none) : 0;
  FC_FUNC_(bind_f90_f_none_object_type_new, BIND_F90_F_NONE_OBJECT_TYPE_NEW)
    (*this, none, &none_len, none_chk_len);
}

FNoneObject::~FNoneObject(void)
{
  FC_FUNC_(bind_f90_f_none_object_free, BIND_F90_F_NONE_OBJECT_FREE)
    (*this);
}

FNoneObject::FNoneObject(void)
{
  FC_FUNC_(bind_f_none, BIND_F_NONE)
    (*this);
}

FProgressBar::FProgressBar(const FProgressBar& other)
{
  FC_FUNC_(bind_f90_f_progress_bar_copy_constructor, BIND_F90_F_PROGRESS_BAR_COPY_CONSTRUCTOR)
    (*this, other);
}

FProgressBar::FProgressBar(const char message[90],
    int ncall,
    int nstep,
    size_t t0)
{
  size_t message_chk_len = 90;
  FC_FUNC_(bind_f90_f_progress_bar_type_new, BIND_F90_F_PROGRESS_BAR_TYPE_NEW)
    (*this, message, &ncall, &nstep, &t0, message_chk_len);
}

FProgressBar::~FProgressBar(void)
{
  FC_FUNC_(bind_f90_f_progress_bar_free, BIND_F90_F_PROGRESS_BAR_FREE)
    (*this);
}

FProgressBar::FProgressBar(const int (*nstep))
{
  FC_FUNC_(bind_f_progress_bar_new, BIND_F_PROGRESS_BAR_NEW)
    (*this, nstep);
}

void FProgressBar::update_progress_bar(int istep)
{
  FC_FUNC_(bind_update_progress_bar, BIND_UPDATE_PROGRESS_BAR)
    (*this, &istep);
}

