    if (.not. ok) then
       if (present(label)) then
          call f_err_throw('"' // label // ": expected " // trim(yaml_toa(expected)) // ", value " // trim(yaml_toa(val)) // '"')
       else
          call f_err_throw('"' // "expected " // trim(yaml_toa(expected)) // ", value " // trim(yaml_toa(val)) // '"')
       end if
    end if
