subroutine f_python_ndarray_init()
  use f_python

  call f_object_add_method("class", "ndarray_new", ndarray_new, 1)
  call f_object_add_method("class", "ndarray_free", ndarray_free, 0)
end subroutine f_python_ndarray_init

subroutine f_python_ndarray_get(arr, data, ndims, shapes, kind)
  use f_python , only: ndarray
  use f_precisions, only: f_address
  implicit none
  type(ndarray), intent(in) :: arr
  integer(f_address), intent(out) :: data
  integer, intent(out) :: ndims
  integer, dimension(7), intent(out) :: shapes
  character(len = 2), intent(out) :: kind

  data = arr%data
  ndims = arr%ndims
  shapes = arr%shapes
  kind = arr%kind
end subroutine f_python_ndarray_get
